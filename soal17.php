<?php 

function perolehan_medali($arr){

    // print_r($arr);
    $medaliCounter = 0;
    $medaliNegara = [];
    $medali = [0, 0, 0];
    $negara = [];

    $assocArr = [];

    //ambli negara
    foreach($arr as $a){
        if(!in_array($a[0], $negara)){
            $negara[] = $a[0];
        }
    }

    // print_r($negara);

    //hitung medali
    foreach($negara as $n){
        foreach($arr as $a){
            if($a[0] == $n){
                if($a[1] == 'emas'){
                    $medali[0]+=1;
                }else if($a[1] == 'perak'){
                    $medali[1]+=1;
                }else if($a[1] == 'perunggu'){
                    $medali[2]+=1;
                }
            }
        }
        $medaliNegara[]=$medali;
        $medali = [0, 0, 0];
    }

    

    for($i=0; $i<count($negara); $i++){
        $assocArr[] = [
            "negara" => $negara[$i],
            "emas" => $medaliNegara[$i][0],
            "perak" => $medaliNegara[$i][1],
            "perunggu" => $medaliNegara[$i][2]

        ];
    }

    return $assocArr;
}

print_r(perolehan_medali(
    array(
        array('Indonesia', 'emas'),
        array('India', 'perak'),
        array('Korea Selatan', 'emas'),
        array('India', 'perak'),
        array('India', 'emas'),
        array('Indonesia', 'perak'),
        array('Indonesia', 'emas')
    )    
));


?>