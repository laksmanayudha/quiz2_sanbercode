SELECT customers.name as customer_name, SUM(orders.amount) as total_amount 
FROM customers INNER JOIN orders ON orders.customer_id = customers.id 
GROUP BY customers.name;